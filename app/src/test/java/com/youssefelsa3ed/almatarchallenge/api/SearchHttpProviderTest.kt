package com.youssefelsa3ed.almatarchallenge.api

import com.youssefelsa3ed.almatarchallenge.utils.URLS
import org.junit.Assert.*
import org.junit.Test
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder

class SearchHttpProviderTest {

    private val queryVal = "some-query"
    private val queryKey = "q"
    private val page = 1
    private val model = SearchDocsModel(queryVal = queryVal)

    @Test
    fun `provideHttpConnection - url`() {

        val searchProvider = SearchHttpProvider()
        val httpConnection = searchProvider.provideHttpConnection(model = model)
        val expectedValue = URL(
            URLS.SearchURL.value +
                    "?${queryKey}=${URLEncoder.encode(queryVal, "UTF-8")}" +
                    "&page=${page}"
        )
        assertEquals(httpConnection.url, expectedValue)
    }

    @Test
    fun `provideHttpConnection - responseCode`() {
        val searchProvider = SearchHttpProvider()
        val httpConnection = searchProvider.provideHttpConnection(model = model)

        assertEquals(httpConnection.responseCode, HttpURLConnection.HTTP_OK)
    }

    @Test
    fun `provideHttpConnection - requestMethod`() {
        val searchProvider = SearchHttpProvider()
        val httpConnection = searchProvider.provideHttpConnection(model = model)

        assertEquals(httpConnection.requestMethod, "GET")
    }
}