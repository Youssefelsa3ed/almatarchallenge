package com.youssefelsa3ed.almatarchallenge.data.services

import androidx.paging.PagingData
import com.youssefelsa3ed.almatarchallenge.model.Doc
import kotlinx.coroutines.flow.Flow

interface SearchRepositoryServices {
    fun getSearchResultStream(queryKey: String, queryVal: String): Flow<PagingData<Doc>>
}